import React from 'react'
import { ContextWrapper }  from './core/context/ContextWrapper'
import Main from './pages/main/main'

function App() {
  return (
    <ContextWrapper>
      <Main />
    </ContextWrapper>
  )
}

export default App
