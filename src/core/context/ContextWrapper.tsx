import React, {  useState } from 'react'
import { DarkModeContext } from './../hooks/DarkModeContext'

export const ContextWrapper = ({children}) => {
    const [darkMode, setDarkMode] = useState(true)

    function toogleDarkMode() {
        setDarkMode(darkMode => !darkMode)
    }
    return (
        <DarkModeContext.Provider value={{ darkMode, toogleDarkMode  }}>     
            {children}
        </DarkModeContext.Provider>
    )
}