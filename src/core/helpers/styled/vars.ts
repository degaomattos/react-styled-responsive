export const COLORS = {
    lightPrimary: "#007f56",
    darkPrimary: "#000",
    lightBackGroundColor: "#ebebeb", 
    darkBackGroundColor: "#000",
    green: "#007f56",
    grey: "#868686",
    red: "#FE9481",
    yellow: "#FCDA92",
    purple: "#9C8Cb9",
    white: "#FFFFFF"
}