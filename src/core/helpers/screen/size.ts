

export const screenSize = {
    xs: `(max-width: 600px)`,
    sm: `(max-width: 960px)`,
    md: `(max-width: 1280px)`,
    lg: `(min-width: 1281px)`,
}