import { createContext } from 'react'

type DarkModeProps = {
    darkMode: boolean
    toogleDarkMode: () => void
}

export const DarkModeContext = createContext({
    darkMode: true,
    toogleDarkMode: () => {}
 } as DarkModeProps)