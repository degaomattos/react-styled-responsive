import styled from 'styled-components'
import { COLORS } from '../../core/helpers/styled/vars'
import { screenSize } from './../../core/helpers/screen/size'

export const ContainerStyles = styled.div `
    max-width: 100%;
    h1 {
        font-size: 4em;
        letter-spacing: -0.05em;
        line-height: 1em;
        text-align: center;
        font-weight: 300;
        color: ${COLORS.green};
        @media ${screenSize.sm} {
            font-size: 2em;
        }
        b {
            font-weight: 800
        }
    }
    h2 {
        font-size: 2em;
        text-align: center;
        font-weight: 300;
        letter-spacing: -0.0625em;
        line-height: 1.1875em;
        margin: 0;
        color: ${COLORS.grey};
        @media ${screenSize.sm} {
            font-size: 1em;
        }
    }
    p {
        font-size: 2em;
        text-align: center;
        font-weight: 300;
        letter-spacing: -0.0625em;
        line-height: 1.1875em;
        color: ${COLORS.grey}; 
        position:relative;
        @media ${screenSize.sm} {
            font-size: 1em;
        }
    }
`
export const SpanStyled = styled.span `
    margin: 15px;
    position: relative;
    &:before {
        content: '';
        display: inline-block;
        width: 15px;
        height: 15px;
        -moz-border-radius: 50%;
        -webkit-border-radius: 50%;
        border-radius: 50%;
        background-color: ${(props: any) => COLORS[props.color]};
        position: absolute;
        top: 10px;
        left: -15px;
    }
`

export const GridContainerStyles = styled.div `
    width: calc(100% + 16px);
    margin: -8px;
    display: flex;
    flex-wrap: wrap;
    box-sizing: border-box;
`

export const GridItemStyles = styled.div `
    flex-grow: 0;
    max-width: 33.3333%;
    flex-basis: 33.3333%;
    padding: 12px;
    margin: 0;
    box-sizing: border-box;
    @media ${screenSize.sm} {
        max-width: 100%;
        flex-basis: 100%;
    }
`

export const ModalStyles = styled.div `
    background-color: #000000e6;
    position: fixed; 
    z-index: 999;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    section {
        max-width: 1280;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
    }
`