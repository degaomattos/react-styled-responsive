import React, { useContext, useState } from 'react'
import { ContainerStyles, SpanStyled, GridContainerStyles, GridItemStyles, ModalStyles } from './container.styles'
import Card from '../card/card'
import { DarkModeContext } from './../../core/hooks/DarkModeContext'
import DesktopImage from './../../assets/images/desktop.png'
import TabletImage from './../../assets/images/tablet.png'
import MobileImage from './../../assets/images/mobile.png'


const cards = [
    {
        title: "Site Responsivo DESKTOP",
        description: "Quando pressionado o botão Leia Mais... O restante da informação deverá aparecer scroll down. aqui aparece o restante do texto.",
        image: DesktopImage, 
        color: "red",
        actionType: 'scrollDown',
        actionLabel: 'Leia mais...'
    },
    {
        title: "Site Responsivo TABLET",
        description: "Quando pressionado o botão Leia Mais... informação deverá aparecer completa em um popup na tela.",
        image: TabletImage, 
        color: "yellow",
        actionType: 'modal',
        actionLabel: 'Leia mais...'
    },
    {
        title: "Site Responsivo MOBILE",
        description: "Quando pressionado o botão alterar tema modifique o tema do site para blackfriday a seu gosto.",
        image: MobileImage, 
        color: "purple",
        actionType: 'toogleDarkMode',
        actionLabel: 'alterar tema...'
    }
]

function Container() {
    const { toogleDarkMode }  = useContext(DarkModeContext)
    const [ sizeDescription , setSizeDescription] = useState(93)
    const [ openModal, setOpenModal ] = useState(false)
    function actionType(actionType){
        switch(actionType){
            case 'toogleDarkMode': 
                return toogleDarkMode
            case 'scrollDown':
                return () => setSizeDescription(-1)
            case 'modal': 
                return () => setOpenModal(!openModal)
        }
    }
    return (
        <ContainerStyles>
            <h1>Crie este site <b>responsivo</b> com <b>react</b> utilizando <b>styled-components</b></h1>
            <h2>A Fonte utilizada é a Open Sans de 300 a 800.</h2>
            <h2>exemplo: "Open Sans", Helvetica, sans-serif, arial;</h2>
            <h2>Já as cores são:</h2>
            <p>
                <SpanStyled color="green">#007f56,</SpanStyled>
                <SpanStyled color="grey">#868686,</SpanStyled>
                <SpanStyled color="red">#FE9481,</SpanStyled> 
                <SpanStyled color="yellow">#FCDA92 e</SpanStyled>
                <SpanStyled color="purple">#9C8CB9</SpanStyled>
            </p>
            <GridContainerStyles>
                {
                    cards.map((item, k) => 
                        <GridItemStyles key={k}>
                            <Card 
                                title={item.title}
                                description={item.actionType === "scrollDown" ? item.description.slice(0, sizeDescription) : item.description}
                                image={item.image}
                                color={item.color}
                                action={
                                    <button onClick={actionType(item.actionType)}>{item.actionLabel}</button>
                                }
                            />
                        </GridItemStyles>
                    )
                }
            </GridContainerStyles>
            {
                openModal && 
                <ModalStyles>
                    <section>
                        <Card 
                            title={cards[1].title}
                            description={cards[1].description}
                            image={cards[1].image}
                            color={cards[1].color}
                            action={
                                <button onClick={() => setOpenModal(!openModal)}>Fechar Modal</button>
                            }
                        />
                    </section>
                </ModalStyles>
            }
        </ContainerStyles>
    )
}

export default Container