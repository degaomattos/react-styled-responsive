import styled from 'styled-components'
import { COLORS } from '../../core/helpers/styled/vars'

export const CardStyles = styled.div `
    background-color: ${COLORS.white};
    figure { 
        padding: 15px;
        margin: 0;
        background-color: ${(props: any) => COLORS[props.color]};
        text-align: center;
        h4 {
            font-size: 1.7777777778em;
            text-align: center;
            font-weight: 300;
            letter-spacing: -0.0625em;
            line-height: 1.1875em;
            color: ${COLORS.white}
        }
    }
    p {
        text-align: justify;
        padding: 10px;
        font-size: 18px
    }

`

export const ActionStyles = styled.div `
    display: flex;
    justify-content: flex-end;
    padding: 10px;
    button {
        color: ${COLORS.white};
        background-color: ${(props: any) => COLORS[props.color]};
        font-size: 16px;
        font-weight: 800;
        padding: 5px;
        border: none;
    }
`
