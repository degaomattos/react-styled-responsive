import React from 'react'
import { CardStyles, ActionStyles } from './card.styles'

interface CardProps {
    image: string 
    color: string
    title: string
    description: string
    action: any
}

function Card({image, title, description, action, color }: CardProps){
    return (
        <CardStyles color={color}>
            <figure>
                <img src={image} />
                <h4>{title}</h4>
            </figure>
            <p>{description}</p>
            <ActionStyles color={color}>
                {action}
            </ActionStyles>
        </CardStyles>
    )
}

export default Card