import styled from 'styled-components'
import { COLORS } from '../../core/helpers/styled/vars'
import { screenSize } from '../../core/helpers/screen/size'

export const FooterStyles = styled.div `
    display: flex; 
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;
    @media ${screenSize.sm} {
        flex-direction: column;
    }
    p {
        font-size: 16px;
        text-align: center;
        font-weight: 300;
        letter-spacing: -0.0625em;
        line-height: 1.1875em;
        color: ${COLORS.grey}; 
    }
`

export const FooterImages = styled.div `
    img {
        margin: 5px;
    }
`