import React from 'react'
import { FooterStyles, FooterImages } from './footer.style'
import RDSmall from  './../../assets/images/rd-small.png'
import RDRaia from  './../../assets/images/drogaraia.png'
import RDDrogasil from  './../../assets/images/drogasil.png'
import RDFarmasil from  './../../assets/images/farmasil.png'
import RDUnivers from  './../../assets/images/univers.png'
import RD4Bio from  './../../assets/images/4bio.png'

function Footer() {
    return (
        <FooterStyles>
            <p>RD 2017. Todos os direitos reservados</p>
            <FooterImages>
                <img src={RDRaia} />
                <img src={RDDrogasil} />
                <img src={RDFarmasil} />
                <img src={RDUnivers} />
                <img src={RD4Bio} />
            </FooterImages>
            <img src={RDSmall} />
        </FooterStyles>
    )
}

export default Footer