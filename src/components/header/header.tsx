import React from 'react'
import { HeaderStyles } from './header.styles'

import RDimage from './../../assets/images/rd.png';

function Header(){
    return(
        <HeaderStyles>
            <img src={RDimage} alt="logo" />
            <nav>
                <ul>
                    <li><a href="#">HTML</a></li>
                    <li><a href="#">CSS3</a></li>
                    <li><a href="#">JAVASCRIPT</a></li>
                    <li><a href="#">REACT</a></li>
                    <li><a href="#">REDUX</a></li>
                </ul>
            </nav>
        </HeaderStyles>
    )
}

export default Header