import styled from 'styled-components'
import { COLORS } from '../../core/helpers/styled/vars'
import { screenSize } from '../../core/helpers/screen/size'

export const HeaderStyles = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid #d5d5d5;
    @media ${screenSize.sm} {
        flex-direction: column;
    }
    nav {
        @media ${screenSize.sm} {
            width: 100%;
        }
        ul {
            display: flex;
            padding-left: 0;
            @media ${screenSize.sm} {
                flex-direction: column;
            }
            li {
                list-style: none;
                padding: 0 10px;
                @media ${screenSize.sm} {
                    background-color: ${COLORS.red};
                    padding: 10px;
                    text-align: center;
                }
                a {
                    color: ${COLORS.green};
                    text-decoration: none;
                    @media ${screenSize.sm} {
                        color: ${COLORS.white};
                    }
                }
            }
        }
    }
`