import React, { useContext } from 'react'
import Header from '../../components/header/header'
import { MainStyles, MainContainer } from './main.styles'
import { DarkModeContext } from '../../core/hooks/DarkModeContext'
import Container from '../../components/container/container'
import Footer from '../../components/footer/footer'

function  Main(){
    const { darkMode } = useContext(DarkModeContext);
    return(
        <MainStyles darkMode={darkMode}>
            <MainContainer >
                <Header />
                <Container />
                <Footer />
            </MainContainer>
        </MainStyles>
    )
}

export default Main