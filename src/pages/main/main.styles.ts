import styled from 'styled-components'
import { COLORS } from './../../core/helpers/styled/vars'

export const MainStyles = styled.div `
    font-family: "Open Sans", Helvetica, sans-serif, arial;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;        
    font-size: 18px;
    font-weight: 300;
    line-height: 1.2222222222em;
    text-align: left;
    width: 100hw;
    min-height: 100vh;
    background-color: ${(props) => props.darkMode ? COLORS.lightBackGroundColor : COLORS.darkBackGroundColor }
`

export const MainContainer = styled.div `
    max-width: 1280px;
    width: 100%;
    display: block;
    box-sizing: border-box;
    margin-left: auto;
    margin-right: auto;
`
